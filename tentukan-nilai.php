<?php
function tentukan_nilai($number)
{
    switch ($number) {
        case ($number >= 85 && $number <= 100):       
            echo "Sangat Baik <br>";
            break;

        case ($number >= 70 && $number < 85):
            echo "Baik <br>";
            break;

        case ($number >= 60 && $number < 70):
            echo "Cukup <br>";
            break;

        default:
            echo 'Kurang <br>';
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
